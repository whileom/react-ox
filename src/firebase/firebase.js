// Initialize Firebase
import * as firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyDqG40MUc73zao2uS3zzIyQTOYvjHiW7eo',
  authDomain: 'react-ox.firebaseapp.com',
  databaseURL: 'https://react-ox.firebaseio.com',
  projectId: 'react-ox',
  storageBucket: 'react-ox.appspot.com',
  messagingSenderId: '450286712747',
};

firebase.initializeApp(config);

firebase
  .database()
  .ref()
  .set({
    name: 'Pichchanok Nuchpan',
    age: 28,
    isSingle: true,
    location: {
      city: 'Bangkok',
      country: 'Thailand',
    },
  })
  .then(() => console.log('Data is saved.'))
  .catch(e => console.log('This failed.', e))

// firebase.database().ref('name').set('Cool Man')
// firebase.database().ref('location/city').set('Pattaya')

firebase
  .database()
  .ref('attributes')
  .set({
    height: '170cm',
    weight: '90km',
  });
// ถ้าไม่มี ref ก็จะสร้างให้ใหม่เลย
