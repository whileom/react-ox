import React from 'react'

const Box = ({id, owner, handleClick}) => (
  <div className='column is-one-third' >
    <div
      className="box"
      onClick={(e) => handleClick(id, owner)}
    >
    { 
      owner === 'o' && 
      <span className="icon has-text-primary">
        <i className="fas fa-3x fa-circle"></i>
      </span>
    }
    { 
      owner === 'x' &&
      <span className="icon has-text-danger">
        <i className="fas fa-3x fa-times has-text-danger"></i>
      </span>
    }
    </div>
  </div>
)

export default Box