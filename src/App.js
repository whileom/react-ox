import React, { Component } from 'react';

import Box from './Box'

const createDefaultBoxs = () => {
  let boxs = []
  for (let i = 0; i<= 8; i++) {
    boxs.push({id: i, owner: undefined})
  }
  return boxs
}

class App extends Component {
  state = {
    boxs: createDefaultBoxs(),
    isPlayerTurn: 'o', //  o and x
    isWin: false
  }

  checkWinnerCondition = (player) => {
    const boxs = [...this.state.boxs]
    let canWin = false
    if (boxs[0].owner === player && boxs[1].owner === player && boxs[2].owner === player) canWin = true
    else if (boxs[3].owner === player && boxs[4].owner === player && boxs[5].owner === player) canWin = true
    else if (boxs[6].owner === player && boxs[7].owner === player && boxs[8].owner === player) canWin = true
    else if (boxs[0].owner === player && boxs[3].owner === player && boxs[6].owner === player) canWin = true
    else if (boxs[1].owner === player && boxs[4].owner === player && boxs[7].owner === player) canWin = true
    else if (boxs[2].owner === player && boxs[5].owner === player && boxs[8].owner === player) canWin = true
    else if (boxs[0].owner === player && boxs[4].owner === player && boxs[8].owner === player) canWin = true
    else if (boxs[2].owner === player && boxs[4].owner === player && boxs[6].owner === player) canWin = true
    return { winner: canWin ? player : undefined }
  }

  callbackHandleClick = (player) => {
    const { winner } = this.checkWinnerCondition(player)
    if (winner) {
      this.setState({ isWin: true })
    }
  }

  restartGame = () => {
    this.setState(() => ({
      boxs: createDefaultBoxs(),
      isPlayerTurn: 'o',
      isWin: false
    }))
  }

  handleClick = (id, owner) => {
    const player = this.state.isPlayerTurn
    if (!this.state.isWin) {
      if (owner === undefined) {
        this.setState({
          boxs: this.state.boxs.map(box => {
            if (box.id === id) {
              box.owner = player
            }
            return box
          }),
          isPlayerTurn: this.state.isPlayerTurn === 'o' ? 'x' : 'o'
        }, () => this.callbackHandleClick(player))
      }
    } else {
      alert('you should new game')
    }
  }

  render() {
    return (
      <section className="hero is-success is-fullheight">
      <div className="hero-body">
        <div className="container">
          <div className="columns">
            <div className="column">
              <h3 className="title is-3">Player Turn is 
                <span className="title is-1">{ this.state.isPlayerTurn }</span>
              </h3>
              { 
                this.state.isWin &&
                <a
                  onClick={this.restartGame}
                  className="button is-success is-inverted"
                >
                  New Game
                </a>
              }
            </div>
            <div className="column">
              <div className="columns is-multiline is-mobile ">
                { this.state.boxs.map(box => (
                  <Box
                    key={box.id}
                    id={box.id}
                    owner={box.owner}
                    handleClick={this.handleClick}
                  />
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    )
}}

export default App;
